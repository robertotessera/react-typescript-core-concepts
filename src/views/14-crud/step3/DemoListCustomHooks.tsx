import React, {
  KeyboardEvent,
  useRef
} from 'react';
import { useFetch } from './use-fetch';

type Item = {
  id: number,
  name: string
}
// https://medium.com/@ryardley/react-hooks-not-magic-just-arrays-cd4f1857236e

const API_URL = 'http://localhost:3001/items';

export const DemoListFetchCustomHooks: React.FC = () => {
  const inputEl = useRef<HTMLInputElement>(null);
  const {data, error, http} = useFetch(API_URL);

  const deleteItem = (itemToDelete: Item) => {
    http.delete(API_URL, itemToDelete.id);
  };

  const addItem =  (event: KeyboardEvent<HTMLInputElement>) => {
    const target = inputEl.current;

    if (event.key === 'Enter' && target) {
      http.post(API_URL, { name: target.value });
      target.value = '';
    }
  };

  return (
    <>
      <h3>SIMPLE LIST: #{data && data.length} Items</h3>
      { error && <div className="alert alert-danger">Server error</div> }

      {/*form*/}
      <input ref={inputEl} type="text" onKeyPress={addItem}/>

      {/*list*/}
      {
        data.map((item: Item, index: number) => {
          return (
            <li className="list-group-item" key={index}>
              {index+1} - {item.name}
              <div className="pull-right">
                <i className="fa fa-trash" onClick={() => deleteItem(item)} />
              </div>
            </li>
          )
        })
      }
    </>
  );
};
