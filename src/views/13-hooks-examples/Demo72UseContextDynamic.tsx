import React, { useContext, useState } from 'react';

const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222"
  }
};

// Create type from initial value (dark or light is the same)
type ThemeProps = typeof themes.dark;

// Define context with initial light
const ThemeContext = React.createContext<ThemeProps>(themes.light);

// root component: level 0
export const DemoUseContextDynamic: React.FC  = () => {
  const [theme, setTheme] = useState(themes.dark)

  return (
    <ThemeContext.Provider value={theme}>
      <Toolbar />

      <hr/>
      <pre>Click to change theme</pre>
      <span
        className="cursor"
        onClick={() => setTheme(themes.light)}>Change to LIGHT Theme</span> |
      <span
        className="cursor"
        onClick={() => setTheme(themes.dark)}> Change to DARK Theme</span>

    </ThemeContext.Provider>
  );
};

// middle component: level 1
const Toolbar: React.FC = () => {
  return (
    <div>
      <ThemedButton label="Button 1" />
      <ThemedButton label="Button 2" />
    </div>
  );
};

// last (leaf) component: level 2
const ThemedButton: React.FC<{ label: string}> = ({label}) => {
  const theme = useContext(ThemeContext);

  const css = {
    background: theme.background,
    color: theme.foreground
  };

  return (
    <button style={css}>
      {label}
    </button>
  );
};
