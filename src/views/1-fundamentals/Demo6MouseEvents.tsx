import React from 'react'

export const DemoMouseEvents: React.FC = () => {

  return (
    <div className="example-box-centered small">
      <Panel title="MouseEvent Demo" url="http://www.google.com">
        Click Icon on header
      </Panel>
    </div>
  )
};

// =========================================
// PANEL SIMPLE COMPONENT WITH MOUSE EVENT
// =========================================

interface PanelProps {
  title: string;
  url?: string;
  children?: React.ReactNode;
}

export const Panel: React.FC<PanelProps> = ({children, url, title}) => {

  function goto(event: React.MouseEvent<HTMLElement>) {
    event.stopPropagation();
    window.open(url);
  }

  return (
    <div className="card">
      <div className="card-header">
        { title }
        { url && <i className="fa fa-link pull-right cursor" onClick={goto} />}
      </div>
      <div className="card-body">{children}</div>
    </div>
  )
};
