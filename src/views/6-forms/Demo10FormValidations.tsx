import React, { ChangeEvent, FormEvent, useState } from 'react';

interface FormData {
  username: string;
  email: string;
  isCompany?: boolean | string;
}

export const SimpleFormDemo: React.FC = () => {
  const [formData, setFormData] = useState<FormData>({ username: '', email: '', isCompany: ''});
  const [dirty, setDirty] = useState<boolean>(false);

  const onChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const target = event.currentTarget;
    setFormData(state => ({
      ...state,
      [target.name]: target.value
    }));
    setDirty(true);
  };

  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    alert(JSON.stringify(formData));
  };

  // validation
  const emailIsValid = validateEmail( formData.email);
  const usernameIsValid = formData.username.length > 3;
  const companyIsValid = formData.isCompany !== '';
  let valid =  emailIsValid && usernameIsValid && companyIsValid;

  return (
    <form onSubmit={submit}>
      <input
        type="text"
        name="username"
        className={'form-control' + (!usernameIsValid && dirty ? ' is-invalid' : '')}
        value={formData.username}
        placeholder="Username (min 3 chars)"
        onChange={onChange}
      />

      <input
        type="text"
        name="email"
        className={'form-control' + (!emailIsValid && dirty ? ' is-invalid' : '')}
        value={formData.email}
        placeholder="email"
        onChange={onChange}
      />

      <select
        name="isCompany"
        onChange={onChange}
        className={'form-control' + (!companyIsValid && dirty ? ' is-invalid' : '')}
      >
        <option value="">Are you a company</option>
        <option value="yes">Yes</option>
        <option value="no">no</option>
      </select>

      <br/>
      <button className="btn btn-primary" disabled={!valid}>REGISTER</button>
    </form>
  )
};

function validateEmail(email: string) {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
