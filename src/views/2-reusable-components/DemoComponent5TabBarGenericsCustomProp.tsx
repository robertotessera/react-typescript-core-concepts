import React, { useState } from 'react';
import classNames from 'classnames';

interface Country  {
  id: number;
  label: string;
  description: string;
}

export const TabBarDemoWithGenericsAndCustomProp: React.FC = () => {
  const countries: Country[] = [
    { id: 1, label: 'Japan', description: 'bla bla 1'},
    { id: 2, label: 'Italy', description: 'bla bla 2'},
  ];
  const [active, setActive] = useState<Country>(countries[0]);

  function onCountryClickHandler(item: Country) {
    setActive(item);
  }

  return (
    <>
      <TabBar
        data={countries}
        onItemClick={onCountryClickHandler}
        active={active}
        labelField="label"
      />
      <hr/>
      { active.description }
    </>
  )
};

/////////////////////////
// TABBAR COMPONENT
/////////////////////////


interface TabBarProps<T> {
  data: T[];
  active: T;
  onItemClick: (item: T) => void;
  labelField: string;
}

interface TabBarItemProps {
  id: number ;
  [key: string]: any;
  // labelField: string
}


export const TabBar = <T extends TabBarItemProps>(props: TabBarProps<T>) => {
// function TabBar <T extends TabBarItemProps>(props: TabBarProps<T>) {
  return (
    <ul className="nav nav-tabs">
      {
        props.data.map((item: T) => {
          const active = item.id ===  props.active.id;
          return (
            <li
              className="nav-item"
              key={item.id}
              onClick={() =>  props.onItemClick(item)}
            >
              <div className={classNames('nav-link', { 'active': active})}>
                {item[props.labelField || 'name']}
              </div>
            </li>
          )
        })
      }
    </ul>
  )
}
